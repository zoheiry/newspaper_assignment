
    $(document).ready(function() {
    	$(".banner .title h4").css('font-size', '84pt');
    	$(".banner .title h4").css('-webkit-transform', 'rotate(1080deg)');
    	$(".banner .title h4").css('-moz-transform', 'rotate(1080deg)');
    	$(".banner .title h4").css('-ms-transform', 'rotate(1080deg)');
    	$(".banner .title h4").css('-o-transform', 'rotate(1080deg)');
    	$(".banner .title h4").css('transform', 'rotate(1080deg)');
  	});

    google.load("feeds", "1");

    var feeds = [
    			 "http://www.digg.com/rss/index.xml",
    			 "https://news.google.com/news/feeds?pz=1&cf=all&ned=us&hl=en&output=rss",
    			 "http://thenextweb.com/feed/",
    			 "http://www.theverge.com/rss/frontpage"
    			];

    var current_length = 0;
    var item_titles = [];
    var item_links = [];
    var item_contentSnippet = [];
    var col_sum = 0;

    function initialize() {
    // var rssAddress = "http://www.digg.com/rss/index.xml";
      for(var f = 0; f < feeds.length; f++ ) {
	      var rssAddress = feeds[f];

	      var feed = new google.feeds.Feed(rssAddress);
	      feed.setNumEntries(100);
	      feed.load(function(result) {
	        if (!result.error) {
	          var container = document.getElementById("feed");
	          for (var i = 0; i < result.feed.entries.length; i++) {
	            var entry = result.feed.entries[i];
	            // var div = document.createElement("div");
	            // div.appendChild(document.createTextNode(entry.contentSnippet));
	            // container.appendChild(div);
	            item_titles.push(entry.title);
	            item_links.push(entry.link);
	            item_contentSnippet.push(entry.contentSnippet);
	          }
	        }
	      });
      }
      setTimeout(function(){
      	createNewsElements();
      }, 1000);
      
    }

    google.setOnLoadCallback(initialize);

    function updateNews() {
    	console.log("INSIDE updateNews()");
    	current_length = item_titles.length;

    	for(var f = 0; f < feeds.length; f++) {
	      var rssAddress = feeds[f];

	      var feed = new google.feeds.Feed(rssAddress);
	      feed.setNumEntries(100);
	      feed.load(function(result) {
	        if (!result.error) {
	          var container = document.getElementById("feed");
	          for (var i = 0; i < result.feed.entries.length; i++) {
	            var entry = result.feed.entries[i];
	            // var div = document.createElement("div");
	            // div.appendChild(document.createTextNode(entry.contentSnippet));
	            // container.appendChild(div);
	            if(item_titles.indexOf(entry.title) == -1) {
		            item_titles.push(entry.title);
		            item_links.push(entry.link);
		            item_contentSnippet.push(entry.contentSnippet);
	        	}
	          }
	        }
	      });
      }
    }
// <div class="col1">
// 			<div class="article">
// 				<div class="article-img">
// 					<div class="image-wrapper">
// 						<img src="images/5.jpg">
// 					</div>
// 				</div>
// 				<div class="article-title">
// 					<h5>THE MOST BEAUTIFUL TYPEFACES FROM THIS PAST MONTH</h5>
// 				</div>
// 				<div class="article-text">
// 					<p>
// 						Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
// 						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
// 						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
// 						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
// 						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
// 						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
// 					</p>
// 				</div>
// 			</div>
// 		</div>
    function createNewsElements() {
    	for(var item_index = current_length; item_index < item_titles.length; item_index++) {
    		createArticle(item_titles[item_index], item_contentSnippet[item_index], item_links[item_index]);
    		console.log("Article #" + item_index);
    	}
    	$(".news-container").css('opacity', 1);
    }

    function createArticle(title, content, link) {
    	$(".news-container").append('<div class="col1">' +
			'<div class="article">' +
				'<div class="article-title">' +
					'<a  href=' + link + ' target="_blank"><h5>' + title + '</h5></a>' +
				'</div>' +
				'<div class="article-text">' +
					'<p>' +
						content +
					'</p>' +
				'</div>' +
			'</div>' +
		'</div>');
    }
